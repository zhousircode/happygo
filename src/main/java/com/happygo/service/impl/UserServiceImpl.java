package com.happygo.service.impl;

import com.happygo.common.Const;
import com.happygo.common.ServerResponse;
import com.happygo.common.TokenCache;
import com.happygo.dao.UserMapper;
import com.happygo.pojo.User;
import com.happygo.service.IUserService;
import com.happygo.util.Md5Util;
import net.sf.jsqlparser.schema.Server;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.provider.MD5;

import java.util.UUID;

@Service("iUserService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ServerResponse<User> login(String username, String password) {
        int status = userMapper.checkUsername(username);
        if (status == 0) {
            return ServerResponse.createSuccessMsg("用户名不存在！！");
        }
        password = Md5Util.MD5EncodeUtf8(password);
        User user = userMapper.selectLogin(username, password);
        if (null == user) {
            return ServerResponse.createErrorMsg("用户名或密码不存在！！");
        }
        //返回值有密码，删除密码
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createSuccessMsg("登录成功！！", user);
    }

    @Override
    public ServerResponse<String> registUser(User user) {
        //判断用户名是否存在
        ServerResponse<String> response = this.checkvalid(user.getUsername(), Const.USERNAME);
        if (!response.isSuccess()) {
            return response;
        }
        //判断邮箱是否已经验证
        response = this.checkvalid(user.getEmail(), Const.EMAIL);
        if (!response.isSuccess()) {
            return response;
        }
        //存储
        user.setRole(Const.Role.ROLE_CUSTOMER);
        user.setPassword(Md5Util.MD5EncodeUtf8(user.getPassword()));
        int status = userMapper.insert(user);
        if (status == 0) {
            return ServerResponse.createSuccessMsg("系统运行异常，数据插入失败！！");
        }
        return ServerResponse.createSuccessMsg("注册成功！！");
    }

    @Override
    public ServerResponse<String> checkvalid(String value, String type) {
        if (StringUtils.isBlank(type)) {
            return ServerResponse.createErrorMsg("参数异常！！");
        }
        int status = 0;
        String msg = "";
        if (Const.EMAIL.equals(type)) {
            status = userMapper.checkEmail(value);
            msg = "邮箱已注册！！";
        } else {
            status = userMapper.checkUsername(value);
            msg = "用户名已存在！！";
        }
        if (status > 0) {
            return ServerResponse.createErrorMsg(msg);
        }
        return ServerResponse.createSuccess();
    }

    @Override
    public ServerResponse<String> checkAnswer(String username, String question, String answer) {
        int sum = userMapper.checkAnswer(username, question, answer);
        if (sum > 0) {
            String token = UUID.randomUUID().toString();
            //缓存tokenn
            TokenCache.putKey("token_" + username, token);
            return ServerResponse.createSuccessMsg(token);
        }
        return ServerResponse.createErrorMsg("安全问题答案错误！！！");
    }

    @Override
    public ServerResponse<String> restPassword(String username,String passwordNew,String token){
        if(StringUtils.isBlank(token)){
            return ServerResponse.createErrorMsg("参数异常！！！");
        }
        //验证token
        String SysToken = TokenCache.getKey("token_"+username);

        if(StringUtils.equals(SysToken,token)){
            //token对比正确就开始修改密码
            passwordNew=Md5Util.MD5EncodeUtf8(passwordNew);
            int stauts=userMapper.chagePassword(username,passwordNew);
            if(stauts>0){
                return ServerResponse.createSuccessMsg("密码修改成功！！！");
            }
            return ServerResponse.createErrorMsg("密码修改失败，请重试！！！");
        }
        return ServerResponse.createErrorMsg("token已过期，或不存在");
    }

}
