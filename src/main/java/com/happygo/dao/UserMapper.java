package com.happygo.dao;

import com.happygo.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.junit.runners.Parameterized;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //根据用户名检查用户是否已经存在
    int checkUsername(String username);
    //登录验证
    User selectLogin(@Param("username") String username,@Param("password") String password);
    //根据邮箱检验邮箱是否已经存在
    int checkEmail(String email);
    //验证安全问题
    int checkAnswer(@Param("username")String username,@Param("question")String question,@Param("answer")String answer);
    //修改密码
    int chagePassword(@Param("username")String username,@Param("password")String password);
}