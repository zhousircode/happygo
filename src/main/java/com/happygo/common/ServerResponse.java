package com.happygo.common;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
//注解，json返回值自动忽略为空的节点
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServerResponse<T> implements Serializable {

    private int status;
    private String msg;
    private T data;

    private ServerResponse(int status) {
        this.status = status;
    }

    private ServerResponse(int status, T data) {
        this.status = status;
        this.data = data;
    }

    private ServerResponse(int status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    private ServerResponse(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }
    //判断是否成功,不需要显示在返回json中
    @JsonIgnore
    public boolean isSuccess() {
        return this.status == ResponseCode.SUCCESS.getCode();
    }
    public int getStatus(){
        return status;
    }
    public T getData(){
        return data;
    }
    public String getMsg(){
        return msg;
    }

    //通过此方法创建一个成功通用对象
    public static <T> ServerResponse<T> createSuccess(){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode());
    }

    //通过此方法创建一个带信息的成功通用对象
    public static <T> ServerResponse<T> createSuccessMsg(String msg){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg);
    }

    //通过此方法创建一个带数据的成功通用对象
    public static <T> ServerResponse<T> createSuccessMsg(T data){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),data);
    }

    //通过此方法创建一个带数据带信息的成功通用对象
    public static <T> ServerResponse<T> createSuccessMsg(String msg,T data){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg,data);
    }

    //通过此方法创建一个失败通用对象
    public static <T> ServerResponse<T> createError(){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode());
    }

    //通过此方法创建一个带信息的失败通用对象
    public static <T> ServerResponse<T> createErrorMsg(String msg){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),msg);
    }

    //通过此方法创建一个带数据的失败通用对象
    public static <T> ServerResponse<T> createErrorMsg(T data){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),data);
    }

    //通过此方法创建一个带数据带信息的失败通用对象
    public static <T> ServerResponse<T> createErrorMsg(String msg,T data){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),msg,data);
    }

    //自定义异常方法
    public static <T> ServerResponse<T> createErrorByCodeMsg(int code,String msg){
        return new ServerResponse<T>(code,msg);
    }

}
