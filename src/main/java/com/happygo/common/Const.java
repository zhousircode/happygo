package com.happygo.common;

/**
 * 常量类
 * create by zhoujun
 * 2018-03-25
 */
public class Const {

    public static final String CURRENT_USER="currentUser";
    public static final String EMAIL="email";
    public static final String USERNAME="username";

    //可以使用内部类对有些值进行分组
    public interface Role{
        int ROLE_CUSTOMER=0;//普通用户
        int ROLE_ADMIN=1; //管理员
    }
}
