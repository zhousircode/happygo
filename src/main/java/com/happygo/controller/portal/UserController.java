package com.happygo.controller.portal;

import com.happygo.common.Const;
import com.happygo.common.ServerResponse;
import com.happygo.pojo.User;
import com.happygo.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * create by zhoujun
 * 2018-03-25
 */
@Controller
@RequestMapping("/user/")
@Api(value = "/user", tags = "User接口")
public class UserController {

    @Autowired
    private IUserService iUserService;

    /**
     * 用户登录
     *
     * @param usrename
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "用户管理", notes = "用户账号密码登录", httpMethod = "POST", response = User.class)
    public ServerResponse<User> login(String usrename, String password, HttpSession session) {
        ServerResponse<User> response = iUserService.login(usrename, password);
        if (response.isSuccess()) {
            session.setAttribute(Const.CURRENT_USER, response.getData());
        }
        return response;
    }

    /**
     * 退出登录
     *
     * @param session
     * @return
     */
    @RequestMapping(value = "logout.do", method = RequestMethod.POST)
    public ServerResponse<String> logout(HttpSession session) {
        session.removeAttribute(Const.CURRENT_USER);
        return ServerResponse.createSuccess();
    }

    /**
     * 注册用户
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "registuser.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> registUser(@RequestBody User user) {
        return iUserService.registUser(user);
    }

    /**
     * 根据类型验证邮箱或账户
     *
     * @param value
     * @param type
     * @return
     */
    @RequestMapping(value = "ckeckvalid.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> checkValid(String value, String type) {
        return iUserService.checkvalid(value, type);
    }

    /**
     * 获得当前登录的用户信息
     *
     * @return
     */
    @RequestMapping(value = "getuserinfo.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> getUserInfo(HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        if (null != user) {
            return ServerResponse.createSuccessMsg("获取成功！！", user);
        }
        return ServerResponse.createErrorMsg("获取用户信息失败！！");
    }

    /**
     * 验证安全问题
     *
     * @return
     */
    @RequestMapping(value = "forgetcheckanswer.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> forgetCheckAnsWer(String username, String question, String answer) {
        return iUserService.checkAnswer(username, question, answer);
    }

    /**
     *
     */
    @RequestMapping(value = "restpassword.do", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> restPassword(String username, String passwordNew, String token) {
        return iUserService.restPassword(username, passwordNew, token);
    }

}