package com.happygo.service;

import com.happygo.common.ServerResponse;
import com.happygo.pojo.User;

/**
 * create by zhoujun
 * 2018-03-25
 */
public interface IUserService {
    ServerResponse<User> login(String username, String password);

    ServerResponse<String> registUser(User user);

    ServerResponse<String> checkvalid(String value, String type);

    ServerResponse<String> checkAnswer(String username, String question, String answer);

    public ServerResponse<String> restPassword(String username,String passwordNew,String token);
}
