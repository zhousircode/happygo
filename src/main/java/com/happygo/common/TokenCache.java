package com.happygo.common;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * guava缓存类
 */
public class TokenCache {
    private static Logger logger = LoggerFactory.getLogger(TokenCache.class);

    //LRU算法
    private static LoadingCache<String, String> localCache = CacheBuilder.newBuilder().initialCapacity(10).maximumSize(100).expireAfterAccess(12, TimeUnit.HOURS).build(
            new CacheLoader<String, String>() {
                //如果利用key取值的时候如果没有取到对应的值，就返回该字段
                @Override
                public String load(String s) throws Exception {
                    return "null";
                }
            }
    );

    public static void putKey(String key, String value) {
        localCache.put(key, value);
    }

    public static String getKey(String key) {
        String value = "";
        try {
            value = localCache.get(key).toString();
            if ("null".equals(value)) {
                return null;
            }
        } catch (ExecutionException e) {
            logger.error("local get cache is exception", e);
        }
        return value;
    }
}
